console.group("Topic: Strings");

// Task 01. padStart
// RU: Объявите три переменных: hour, minute, second.
//     Присвойте им следующие значения: 4, 35, 5.
//     Выведите в консоль время в формате 04:35:05.
// EN: Declare three variables: hour, minute, second.
//     Assign them the following values: 4, 35, 5.
//     Display the time in the format 04:35:05 in the console.

let hour = '4';
let minute = '35';
let second = '5';

let data = hour.padStart(2,'0') + minute.padStart(3,':0') + second.padStart(3, ':0');

console.log(data);

// Task 02. repeat
// RU: Создайте функцию, которая выведет в консоль пирамиду на 9 уровней как показано ниже
//     1
//     22
//     333
//     4444
//     ...
// EN: Create a function which displays a 9 level pyramid in the console according to the
//     following pattern
//     1
//     22
//     333
//     4444
//     ...

function pyramid(levels) {

    for(let i = 1; i <= levels; i++) {

        let str = String(i);
        
        console.log(str.repeat(i));
        
    }
}

pyramid(9);


// Task 03. includes
// RU: Напишите код, который выводит в консоль true, если строка str содержит
//     'viagra' или 'XXX', а иначе false.
//     Тестовые данные: 'buy ViAgRA now', 'free xxxxx'
// EN: Create a snippet of code which displays the value true in the console
//     when str contains 'viagra' or 'XXX', otherwise it displays false.

function addWord(str) {
    let text1 = "XXX";
    let text1Lower = text1.toLowerCase();
    let text2 = "viagra";
    let string = str.toLowerCase();

    console.log(string.includes(text1Lower));
    console.log(string.includes(text2));

}

addWord("buy ViAgRA now");

// Task 04. includes + index
// RU: Проверить, содержит ли строка второе вхождение подстроки,
//     вернуть true/false.
// EN: Check whether the string contains a second occurrence of a substring,
//     return true / false.

let str = 'содержит ли строка второе вхождение подстроки';
let text = 'од';
let oneEnter = str.indexOf(text);

if(str.indexOf(text, oneEnter + 1)) {
    console.log('true');
}

// Task 05. Template literal
// RU: Создать строку: "ten times two totally is 20"
//     используя переменные:
//     const a = 10;
//     const b = 2;
//     и template literal
// EN: Create s string "ten times two totally is 20"
//     using the following variables:
//     const a = 10;
//     const b = 2;
//     and template literal

const a = 10;
const b = 2;
const str = "ten times two totally is";

const string = `${str} ${a*b}`;

console.log(string);

// Task 06. normalize
// RU: Создайте функцию, которая сравнивает юникод строки.
//     Сравните две строки
//     var str1 = '\u006d\u0061\u00f1';
//     var str2 = '\u006d\u0061\u006e\u0303';
// EN: Create a function that compares the unicode strings.
//     Compare 2 strings:
//     var str1 = '\u006d\u0061\u00f1';
//     var str2 = '\u006d\u0061\u006e\u0303';

function getUnicod() {
    let str1 = '\u006d\u0061\u00f1';
    let str2 = '\u006d\u0061\u006e\u0303';

    let str1Norm = str1.normalize();
    let str2Norm = str2.normalize();

    console.log(str1Norm == str2Norm);
}

getUnicod();

// Task 07. endsWith
// RU: Создайте функцию, которая на вход получает массив имен файлов и расширение файла
//     и возвращает новый массив, который содержит файлы указанного расширения.
// EN: Create a function that gets an array of file names and a file extension as its parameters
//     and returns a new array that contains the files of the specified extension.

let array = [{name: 'picture', fileExtension: '.png'}, 
            {name: 'presentation', fileExtension: '.ppx'},
            {name: 'picture1', fileExtension: '.png'}
            ];

function getFileExtension() {
    const arr = array.filter(array => array.fileExtension == ".png");
    console.log(arr);
} 

getFileExtension(array);

// Task 08. String.fromCodePoint
// RU: Создать функцию, которая выводит в консоль строчку в формате 'символ - код'
//     для кодов в диапазоне 78000 - 80000.
// EN: Create a function that displays a line in the format 'character - code' to the console
//     for codes in the range of 78000 - 80000.

function getSymbolCode(a, b) {
    for(let i = a; i <= b; i++) {
        console.log(i + " " + String.fromCodePoint(i));
    }
}

getSymbolCode(78000, 80000);

// Task 09
// RU: Создайте функцию, которая должна выводить в консоль следующую пирамиду
//     Пример:
//     pyramid(1) = '#'
//
//     pyramid(2) = ' # '
//                  '###'
//
//     pyramid(3) = '  #  '
//                  ' ### '
//                  '#####'
// EN: Create a function that should display the next pyramid in the console
//     Example:
//     pyramid(1) = '#'
//
//     pyramid(2) = ' # '
//                  '###'
//
//     pyramid(3) = '  #  '
//                  ' ### '
//                  '#####'

function pyramid(levels) {

    let output="";

    for (var i = 0; i < n; i++) {

    let myspace = "";   

        for(let j = 0; j <(n-i-1); j++) {
            myspace += " ";
        }
            for (let j = 1; j <= 2*i + 1; j++) {
                output += "#";

            }
            output="";
            console.log(myspace + output);
}

pyramid(3);

// Task 10
// RU: Создайте тег-функцию currency, которая формитирует числа до двух знаков после запятой
//     и добавляет знак доллара перед числом в шаблонном литерале.
// EN: Create a currency tag function that forms numbers up to two decimal digits.
//     and adds a dollar sign before the number in the template literal.

console.groupEnd();
