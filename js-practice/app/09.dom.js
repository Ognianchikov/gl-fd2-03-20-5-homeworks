console.group("Topic: DOM");

// Task 01
// Найти элемент с id= "t01". Вывести в консоль.
// Найти родительский элемент и вывести в консоль.
// Найти дочерние ноды, если они есть, и вывести в консоль
// названия и тип нод.
const elem = document.getElementById("t01");
const parentEl = elem.parentElement;
const childrenElems = elem.childNodes;


console.log(elem);
console.log(parentEl);
console.log(childrenElems);

// Task 02
// Подсчитать количество <li> элементов на странице. Для поиска элементов использовать
// getElementsByTagName(). Вывести в консоль.
// Добавить еще один элемент в список и вывести снова их количество.

const liSum1 = document.getElementsByTagName('li').length;

console.log(liSum1);

const newLi = document.createElement('li');

document.body.append(newLi);

const liSum2 = document.getElementsByTagName('li').length;

console.log(liSum2);


// Task 03
// Получить элементы <li> используя метод querySelectorAll() и вывети их в консоль
// Добавить новый <li> и снова вывести в консоль

const liSum1 = document.querySelectorAll('li').length;

console.log(liSum1);

const newLi = document.createElement('li');

document.body.append(newLi);

const liSum2 = document.querySelectorAll('li').length;

console.log(liSum2);

// Task 04
// Найти все первые параграфы в каждом диве и установить цвет фона #ffff00

function makeNewColor() {

    const el = document.querySelectorAll('div');
    const firstElem = el.firstChild;
    return firstElem.style.backgroundColor = '#ffff00';
}

makeNewColor();

// Task 05
// Подсчитать сумму строки в таблице и вывести ее в последнюю ячейку

function myFunction() {

    var rowSumm = 0;
    var table = document.getElementById("table");
  
    for (var i = 0, row; row = table.rows[0]; i++) {
      rowSumm = 0;     
      for (var j = 0, col; col = row.cells[j]; j++) {
        rowSumm += parseFloat(col.firstChild.nodeValue);     
      }
    }
    row.cells[j].innerHTML = rowSumm;  
  }

// Task 06
// Вывести значения всех атрибутов элемента с идентификатором t06

const el = document.querySelector('t06');
const atr = el.attributes;

console.log(atr)

// Task 07
// Получить объект, который описывает стили, которые применены к элементу на странице
// Вывести объект в консоль. Использовать window.getComputedStyle().

const el = document.createElement('div');
document.body.append(el);
const style = window.getComputedStyle(el);

console.log(style);

// Task 08
// Установите в качестве контента элемента с идентификатором t08 следующий параграф
// <p>This is a paragraph</>

const el = document.createElement('div');
const p = document.createElement('p');

el.classList.add('t08');
p.textContent = 'This is a paragraph';

document.body.append(el);
document.getElementsByClassName('t08').append(p);

// Task 09
// Создайте элемент <div class='c09' data-class='c09'> с некоторым текстовым контентом, который получить от пользователя,
// с помощью prompt, перед элементом с идентификатором t08,
// когда пользователь кликает на нем

const el = document.createElement('div');

el.classList.add('c09');
el.setAttribute('data-class', 'c09');
el.textContent = newText;

document.body.append(el);

function addWord() {
    const newText = prompt('Введите текст:');
    return newText;
}

document.getElementsByClassName('c09').addEventListener('click', addWord)

// Task 10
// Удалите у элемента с идентификатором t06 атрибут role
// Удалите кнопку с идентификатором btn, когда пользователь кликает по ней

const el = document.createElement('div');
const btn = document.createElement('button');

el.setAttribute('role', 'dc');
el.classList.add('t06')

btn.classList.add('btn');

document.body.append(el, btn);

function deleteBtn(btn) { 
    return btn.remove();    
}

document.getElementsByClassName('t06').removeAttribute('role');
document.getElementsByClassName('btn').addEventListener('click', deleteBtn)

console.groupEnd();
