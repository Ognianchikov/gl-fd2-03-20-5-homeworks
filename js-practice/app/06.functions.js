console.group("Topic: Functions");

// Task 1. FDS
// RU: Создайте функцию conc, которая должна конкатенировать значения
//     двух параметров a и b и возвращать строку.
//     Используйте Function Declaration Statement (FDS).
//     Вызовите функцию до ее объявления.
//     Тестовые данные:
//     a = '1', b = '1', result = '11'
//     a = 1, b = 1, result = '11'
// EN: Create a function conc, which should concatenate the values
//     of two parameters a and b and return a string.
//     Use Function Declaration Statement (FDS).
//     Call a function before it declaration.
//     Test data:
//     a = '1', b = '1', result = '11'
//     a = 1, b = 1, result = '11'

let result = "";

conc(1, 1);

function conc(a,b) {
    return result = `${a}${b}`;

// Task 2. FDE
// RU: Создайте функцию comp, которая должна сравнивать значения
//     двух параметров a и b и возвращать 1, если они равны и -1, если они не равны.
//     Используйте Function Definition Expression (FDE).
//     Вызовите функцию до ее объявления.
//     Тестовые данные:
//     a = 'abc', b = 'abc', result = 1
//     a = 'abC', b = 'abc', result = -1
// EN: Create a function comp, which should compare the values
//     of two parameters a and b, and return 1, when they equal and return -1,
//     when they are not equal.
//     UseFunction Definition Expression (FDE).
//     Call a function before it declaration.
//     Test data:
//     a = 'abc', b = 'abc', result = 1
//     a = 'abC', b = 'abc', result = -1

let result;

comp('abc', 'abc');

function comp(a, b) {

    if(a == b) {
        result = 1;
    } else {
        result = -1;
    }
}

console.log(result);

// Task 3. AF
// RU: Создайте анонимную функцию, которая должна
//     выводить сообщение 'message in console' в коноль.
//     Используйте ее как обработчик события click для кнопки.
// EN: Create an anonymous function, which should display
//     message 'message in console' in console.
//     Use it as an event handler of event click of the button.

function onClick() {
    console.log( 'message in console' );
  }
  
  button.addEventListener("click",  onClick());

// Task 4. NFE
// RU: Создайте функцию fibo, которая должна вычислять числа Фибоначчи по формуле
//     F0 = 0, F1 = 1, Fn = Fn-1 + Fn-2.
//     Создайте функцию factorial, которая должна вычислять факториал числа по формуле
//     Fn = 1 * 2 *..*n.
//     Используйте Named Function Expression (NFE).
// EN: Create a function fibo should calculate Fibonacci numbers according to the formula
//     F0 = 0, F1 = 1, Fn = Fn-1 + Fn-2.
//     Create a function factorial which should calculate factorial of a number n
//     according to the formula Fn = 1 * 2 *..*n.
//     Use Named Function Expression (NFE).

function fibo(n) {
    if (n <= 2) {
        return 1;
    }
    return fibo(n - 1) + fibo(n - 2);
}

function factorial(n) {
    if (n <= 1) {
      return 1;
      }
    
      return n * factorial(n - 1);
  };
  
console.log(fibo(1));
console.log(factorial(3));

// Task 5. FC
// RU: Объявите две строчные переменные: params и body и проинициализируйте
//     их строчными значениями, которые представляют список параметров и тело будущей функции.
//     Создайте функцию, используя эту информацию с помощью Function Constructor (FC).
//     Вызовите эту функцию.
// EN: Declare two string variables: params and body and initialize them with string values,
//     which represent the list of parameters and the body of future function.
//     Create function using these variables with help of Function Constructor (FC).
//     Call this function.

let params = ['a', 'b'];
let body = 'return a + b';

const sum = new Function(params, body);

console.log(sum(1, 2))

// Task 6. ArF
// RU: Объявите массив arr = [1, 8, 3, 5, 12, 7, 9, 11]
//     Используя стрелочные функции создайте новый массив из элементов elem * elem,
//     которые меньше 100 и отсортируйте его по возрастанию.
//     Выведите результат в консоль.
// EN: Declare an array arr = [1, 8, 3, 5, 12, 7, 9, 11]
//     Using arrow functions create new array which contains elem * elem.
//     These elements should be less than 100, sort it in ascending order.
//     Display the result in the console.

let arr = [1, 8, 3, 5, 12, 7, 9, 11];
let newArr = arr.map(elem => elem*elem ).sort((a,b) => a - b).filter(a => a < 100);

console.log(newArr);

// Task 7. IIFE
// RU: Создайте конструкцию, с помощью которой выполниться выше реализованная
//     функция conc.
// EN: Create a construction which allows to run the above function conc.

(function conc(){
    let arr = [1, 8, 3, 5, 12, 7, 9, 11];
    let newArr = arr.map(elem => elem*elem ).sort((a,b) => a - b).filter(a => a < 100);
    
    console.log(newArr);
})();

// Task 8. Arguments Object, Rest
// RU: Создайте функцию parts, которая принимает неизвестное количество параметров.
//     Каждый параметр – это группа предложений.
//     Функция должна вырезать из параметра подстроку, начиная с символа двоеточие (:)
//     и заканчивая символом точка (.).
//     Функция должна возвращать массив подстрок.
//     Используйте Function Definition Expression (FDE).
//     Тестовые данные:
//     param1 = "This is the first sentence. This is a sentence with a list of items:
//               cherries, oranges, apples, bananas."
//     param2 = "This is the second sentence. This is a sentence with a list of items:
//               red, blue, yellow, black."
//     result = ["cherries, oranges, apples, bananas", "red, blue, yellow, black"].
// EN: Create a function parts, which takes unknown quantity of parameters.
//     Each parameter is a group of sentances.
//     The function should cut out the substring from the parameter, starting with the colon (:)
//     and ending with a period (.) character.
//     The function should return an array of substrings.
//     Use Function Definition Expression (FDE).
//     Test Data:
//     param1 = "This is the first sentence. This is a sentence with a list of items:
//               cherries, oranges, apples, bananas."
//     param2 = "This is the second sentence. This is a sentence with a list of items:
//               red, blue, yellow, black."
//     result = ["cherries, oranges, apples, bananas", "red, blue, yellow, black"].

function parts(...arguments) {
    let args = arguments;
    let result = args.map(elem => elem.slice(elem.lastIndexOf(':') + 1, elem.lastIndexOf('.') + 1));

    console.log(result);
}

const param1 = "This is the first sentence. This is a sentence with a list of items: cherries, oranges, apples, bananas.";
const param2 = "This is the second sentence. This is a sentence with a list of items: red, blue, yellow, black.";

parts(param1, param2);



// Task 9. Optional Arguments
// RU: Создайте функцию find(testString, test), которая должна возвращать позицию
//     строки test в строке testString.
//     Если второй параметр не задан, используйте test = testString.
//     Используйте Function Definition Expression (FDE).
//     Тестовые данные:
//     testString = 'abc', test ='b', result = 1
//     testString = 'abc', result = 0
//     testString = 'abc', test = 'd', result = -1
//     testString = 'abc', test='a', test2='b', result = 0
// EN: Create a function find(testString, test), which should return the position
//     of test string within testString.
//     If the second parameter is omitted, use default value  test = testString.
//     Use Function Definition Expression (FDE).
//     Test Data:
//     testString = 'abc', test ='b', result = 1
//     testString = 'abc', result = 0
//     testString = 'abc', test = 'd', result = -1
//     testString = 'abc', test='a', test2='b', result = 0

function find(testString, test, test2) {

    if(test === undefined) {
        test = testString;
        return result = 0;
    } if(testString.indexOf(test) === 1) {
        return result = 1;
    } if(testString.indexOf(test) < 0) {
        return result = -1;
    } if(test, test2) {
        return result = 0;
    }
}

console.log(find('abc'))
console.log(find('abc', 'b'))
console.log(find('abc', 'd'))
console.log(find('abc', 'a', 'b'))

// Task 10. Function as an Object
// RU: Создайте функцию str(), которая принимает один строчный параметр и
//     выводит в консоль 'String is non empty', если параметр - непустая строка и
//     'String is empty', если параметр – пустая строка.
//     Создайте функцию str.isNonEmptyStr(), как свойство функции str. Эта функция должна
//     принимать один параметр и возвращать true, если параметр непустая строка,
//     иначе false. Используйте эту функцию для реализации условия в основной функции.
//     Тестовые данные:
//     str.isNonEmptyStr(), result = false
//     str.isNonEmptyStr(''), result = false
//     str.isNonEmptyStr('a'), result = true
//     str.isNonEmptyStr(1), result = false
//     str(), console.log('String is empty')
//     str('a'), console.log('String is non empty')
// EN: Create a function str(), which takes one string parameter and display in the console
//     string 'String is non empty' if the paramer is not empty string, otherwise it
//     should display 'String is empty'.
//     Create a function str.isNonEmptyStr() as a property of function str. This function
//     should take one parameter and return true, when the value of parameter is not empty
//     string, otherwise it should return false. Use this function to implement if statement
//     in the str() funtion.
//     Test Data:
//     str.isNonEmptyStr(), result = false
//     str.isNonEmptyStr(''), result = false
//     str.isNonEmptyStr('a'), result = true
//     str.isNonEmptyStr(1), result = false
//     str(), console.log('String is empty')
//     str('a'), console.log('String is non empty')

let str = function str(param) {
    if(param === undefined) {
        console.log('String is empty');
    } else {
        console.log('String is non empty');
    }
};
  
function isNonEmptyStr(param) {
    if((param === undefined) || (param.length === 0) || (typeof param === "number")) {
        return result = false, console.log(result);
    } else {
        return result = true, console.log(result);
    }
};

str.isNonEmptyStr = isNonEmptyStr;

str.isNonEmptyStr();
str.isNonEmptyStr('');
str.isNonEmptyStr('a');
str.isNonEmptyStr(1);
str();
str('a');


// Task 11. Function as a Parameter
// RU: Создайте функцию toConsole с одним параметром. Функция должна выводить
//     значение параметра в консоль.
//     Создайте функцию toAlert с одним параметром. Функция должна выводить значение
//     параметра используя alert().
//     Создайте функцию splitToWords с двумя параметрами: msg и callback.
//     Функция должна разделять строку на слова и использовать колбек для отображения слов.
//     Если второй параметр не задан, функция должна возвращать массив слов.
// EN: Create a function toConsole with one parameter. The function should display
//     the value of the parameter in the console.
//     Create a function toAlert with one parameter. The function should display
//     the value of the parameter using alert.
//     Create a function splitToWords, which takes two parameters: msg и callback.
//     The function should split the value of parameter msg by the words and use callback
//     to display these words.
//     If the second parameter is omitted, the function should return array of words.
//     Test Data:
//     splitToWords("My very long text msg", toConsole);
//     result:
//     My
//     very
//     long
//     text
//     msg
//     splitToWords("My very long text msg", toAlert);
//     result = alert(My), ….
//     console.log( splitToWords("My very long text msg") );
//     result = ['My', 'very', 'long', 'text', 'msg']

function toConsole(msg) {
    return console.log(msg);
}

function toAlert(msg) {
    return alert(msg);
}

function splitToWords(msg, callback) {
        if(typeof callback == "undefined") {
            result = msg.split(' ');
            return console.log(result);
        } else {
            callback(msg.split(' ').join("\r\n"));
        }

}

splitToWords("My very long text msg", toConsole);
splitToWords('My very long text msg', toAlert);
splitToWords("My very long text msg");

// Task 12. Function as a Result
// RU: Создайте функцию copyright, которая должна возвращать другую функцию с
//     одним параметром. Возращаемая функция должна прибавлять знак © ('\u00A9')
//     вначале своего параметра и возвращать результат. Объявите этот знак в функции copyright.
//     Тестовые данные:
//     console.log( copyright()('EPAM') ); result = © EPAM.
// EN: Create a function copyright, which ahould return anther function with one parameter.
//     This returned function should prepend sign © ('\u00A9') to its parameter and
//     return the result. Declare the sign © ('\u00A9') inside copyright function.
//     Test Data:
//     console.log( copyright()('EPAM') ); result = © EPAM.

function copyright(a) {
  
    let result = '\u00A9 ';
    
    function fn(a) {
      result = result + a;
      return result;
    }

    return fn;
}

console.log(copyright()('EPAM'));

// Task 13. Function as a Result
// RU: Задание аналогично предыдущему, но в этот раз функция copyright получает знак
//     как свой параметр.
// EN: This task is similar to the previous one, but in this case the function copyright takes
//     one parameter - sign (© ('\u00A9')).

function copyright(a) {
  
    let result = ' EPAM';
    
    function fn(a) {
      result = a + result;
      return result;
    }

    return fn;
}

console.log(copyright()('\u00A9'));

// Task 14. Function as a Method
// RU: Создайте литерал объекта employee со следующими свойствами:
//     name: 'Ann',
//     work – функция, которая выводит в консоль сообщение "I am Ann. I am working..."
//     Тестовые данные
//     employee.work()  результат в консоле "I am Ann. I am working..."
// EN: Create an object literal employee with the following properties:
//     name: 'Ann',
//     work – function, which display in the console the following string
//     "I am Ann. I am working..."
//     Test Data:
//     employee.work()  result in the console "I am Ann. I am working..."

const employee = {
    name: 'Ann',
    work() {
        console.log(`I am ${this.name}. I am working...`)
    }
}

employee.work();

// Task 15. Borrow Method
// RU: Создайте литерал объекта person со свойством name.
//     Вызовите метод work объекта employee из предыдущего задания.
// EN: Create an object literal person with property name.
//     Call the method work of the object employee from the previous task.

const employee = {
    name: 'Ann',
}

const person = {
    name: 'Mikita'
}

function work() {
    console.log(`I am ${this.name}. I am working...`)
}

employee.work = work;
person.work = work;

employee.work();
person.work();

// Task 16. Memoization
// RU: Создать функцию fiboMemo для вычисления чисел Фибоначчи по формуле
//     F0 = 0, F1 = 1, Fn = Fn-1 + Fn-2. Функция должна хранить те значения,
//     которые она уже вычисляла. Используя методы console.time(), console.timeEnd()
//     определите время вычисления функции fibo и функции fiboMemo.
// EN: Create a function fiboMemo for calculating Fibonacci numbers according to the formula
//     F0 = 0, F1 = 1, Fn = Fn-1 + Fn-2. The function should store the values computed earlier.
//     Using methods console.time(), console.timeEnd() calculate the time for function fibo
//     and fiboMemo.
console.time('fiboMemo')
function fiboMemo(fn) {
    const memo = {};
    
    return function() {
      //arguments
      const keyPrefix = '';
      const key = [...arguments].reduce(
        (acc, argi) => acc + ':' + argi,
        keyPrefix
      ).slice(1);
    
      if (key in memo) {
        console.log('если значение ранее не было посчитано ->  считаем -> запись в кэш');
        return memo[key];
      } else { 
        console.log('если значение было ранее посчитано -> берем из кэша');
        const value = fn(...arguments);
        memo[key] = value;
        return value;
      }
    }
  }
console.timeEnd('fiboMemo')

console.time('fibo')
  function fibo(n) {
    if (n <= 2) {
        return 1;
    }
    return fibo(n - 1) + fibo(n - 2);
  }
console.timeEnd('fibo')
  
  const memoizedFibo = fiboMemo(fibo);
  
  console.log(memoizedFibo(7));
  console.log(memoizedFibo(8));
  console.log(memoizedFibo(7));
console.groupEnd();
