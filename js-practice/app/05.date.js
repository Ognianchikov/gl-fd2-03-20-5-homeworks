console.group("Topic: Date object");

// Task 1
// RU: Создать текущую дату и вывести ее в формате dd.mm.yyyy и dd Month yyyy
// EN: Create current date and display it in the console according to the format
//     dd.mm.yyyy и dd Month yyyy

let today = new Date(); 
let today1 = new Date(); 

let dd = today.getDate();
let mm = today.getMonth() + 1;
let yyyy = today.getFullYear();

if(dd<10){dd='0'+dd}; 
if(mm<10){mm='0'+mm}; 

today = dd+'.'+mm+'.'+yyyy;

Date.prototype.toShortFormat = function() {

    let month_names =["Jan","Feb","Mar",
                      "Apr","May","Jun",
                      "Jul","Aug","Sep",
                      "Oct","Nov","Dec"];

    let dd = today1.getDate();
    let yyyy = today1.getFullYear();
    let month_index = this.getMonth();

    return "" + dd + "-" + month_names[month_index] + "-" + yyyy;
}

console.log(today);
console.log(today1.toShortFormat());

// Task 2
// RU: Создать объект Date из строки '15.03.2025'.
// EN: Create an object Date from the string '15.03.2025'.

let str = '15:03:2025';
let today = new Date(str);

alert(today);

// Task 3
// RU: Создать объект Date, который содержит:
//     1. завтрашнюю дату,
//     2. первое число текущего месяца,
//     3. последнее число текущего месяца
// EN: Create an object Date, which represents:
//     1. tomorrow
//     2. first day of the current month
//     3. last day of the current month

const date1 = new Date('March 11, 2020');
const date2 = new Date('March 1, 2020');
const date3 = new Date('March 31, 2020');

console.log(date1, date2, date3)

// Task 4
// RU: Подсчитать время суммирования чисел от 1 до 1000.
// EN: Calculate the time of summing numbers from 1 to 1000.
``
const start= new Date().getTime();

function sumTo(n) {
    return n * (n + 1) / 2;
  }

sumTo(1000);

const end = new Date().getTime();

console.log(`${end - start}ms`);

// Task 5
// RU: Подсчитать количество дней с текущей даты до Нового года.
// EN: Calculate the number of days from the current date to the New Year.

const date1 = new Date();
const date2 = new Date('01-01-2021');
let daysLag = Math.ceil(Math.abs(date2.getTime() - date1.getTime()) / (1000 * 3600 * 24));
console.log(daysLag)

console.groupEnd();
