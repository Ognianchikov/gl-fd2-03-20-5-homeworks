console.group("Topic: Primitive Data Types");

// Task 01
// Объявите переменную days и проинициализируйте ее числом от 1 до 10.
// Преобразуйте это число в количество секунд и выведите в консоль.

const days = +9;
console.log(days*60*60*24);

// Task 02
// Объявите две переменные: admin и name. Установите значение переменной name
// в ваше имя. Скопируйте это значение в переменную admin и выведите его в консоль.

let admin = 'Mikita';
let name = admin;

console.log(name);

// Task 03
// Объявите три переменных: a, b, c. Присвойте им следующие значения: 10, 2, 5.
// Объявите переменную result1 и вычислите сумму значений переменных a, b, c.
// Объявите переменную min и вычислите минимальное значение переменных a, b, c.
// Выведите результат в консоль.

let a = 10;
let b = 2;
let c = 5;
let result1 = a + b + c;

console.log(result1);

// Task 04
// Объявите три переменных: hour, minute, second. Присвойте им следующие значения:
// 10, 40, 25. Выведите в консоль время в формате 10:40:25.

let hour = 10;
let minute = 40;
let second = 25;

console.log(hour + ':' + minute + ':' + second);

// Task 05
// Объявите переменную minute и проинициализируйте ее целым числом.
// Вычислите к какой четверти принадлежит это число и выведите в консоль.

let minute = 40;
let ch = minute / 15;

console.log(ch);

// Task 06
// Объявите две переменные, которые содержат стоимость товаров:
// первый товар - 0.10 USD, второй - 0.20 USD
// Вычислите сумму и выведите в консоль. Используйте toFixed()

let product1 = 0.10;
let product2 = 0.20;

let sum = product2 + product1;

console.log(sum.toFixed(2) + " USD");


// Task 07
// Объявите переменную a.
// Если значение переменной равно 0, выведите в консоль "True", иначе "False".
// Проверьте, что будет появляться в консоли для значений 1, 0, -3.

let a = 0;

    if(a = 0) {
        console.log("True");
    } else {
        console.log("False");
    }

// Task 08
// Объявите две переменных: a, b. Вычислите их сумму и присвойте переменной result.
// Если результат больше 5, выведите его в консоль, иначе умножте его на 10
// и выведите в консоль.
// Данные для тестирования: 2, 5 и 3, 1.

let a = 3;
let b = 1;

let result = a + b;

if(result > 5) {
    console.log(result);
} else {
    console.log(result*10);
}

// Task 09
// Объявите переменную month и проинициализируйте ее числом от 1 до 12.
// Вычислите время года и выведите его в консоль.

let month = 9;

switch (month) {
    case 1:
        console.log("Зима");
        break;
    case 2:
        console.log("Зима");
        break;
    case 3:
        console.log("Весна");
        break;
    case 4:
        console.log("Весна");
        break;
    case 5:
        console.log("Весна");
        break;
    case 6:
        console.log("Лето");
        break;
    case 7:
        console.log("Лето");
        break;
    case 8:
        console.log("Лето");
        break;
    case 9:
        console.log("Осень");
        break;
    case 10:
        console.log("Осень");
         break;
    case 11:
        console.log("Осень");
        break;
    case 12:
        console.log("Зима");
        break;
    default:
        console.log("Нет таких значений");
}

// Task 10
// Выведите в консоль все числа от 1 до 10.

for(let number = 1; number <= 10; number++) {
    console.log(number);
}

// Task 11
// Выведите в консоль все четные числа от 1 до 15.

for(let number = 1; number <= 15; number++) {
    if(number % 2 == 0) {
        console.log(number);
    }
}

// Task 12
// Нарисуйте в консоле пирамиду на 10 уровней как показано ниже
// x
// xx
// xxx
// xxxx
// ...

function pyramid(levels) {

    let symbol = 'X';
    
    for(let i = 1; i <= levels; i++) {
        console.log(symbol += 'X');
    }
}

pyramid(10);

// Task 13
// Нарисуйте в консоле пирамиду на 9 уровней как показано ниже
// 1
// 22
// 333
// 4444
// ...

function pyramid(levels) {

    for(let i = 1; i <= levels; i++) {

        let str = i;
        
        for(let j = 1; j < i; j++) {
            str += [i];
        }
        console.log(str);
        
    }
}

pyramid(9);

// Task 14
// Запросите у пользователя какое либо значение и выведите его в консоль.

let name = prompt("Как Вас зовут?");

console.log(name);

// Task 15
// Перепишите if используя тернарный опертор
// if (a + b < 4) {
//   result = 'Мало';
// } else {
//   result = 'Много';
// }

let a = 3;
let b = 4;

let age = (a + b < 4) ? console.log('Мало') : console.log('Много');

// Task 16
// Перепишите if..else используя несколько тернарных операторов.
// var message;
// if (login == 'Вася') {
//   message = 'Привет';
// } else if (login == 'Директор') {
//   message = 'Здравствуйте';
// } else if (login == '') {
//   message = 'Нет логина';
// } else {
//   message = '';
// }

let login = prompt('Вы кто?');

let message = (login == 'Вася') ? console.log('Привет') :
    (login == 'Директор') ?  console.log('Здравствуйте') :
    (login == '') ?  console.log('Нет логина') :  console.log('');

// Task 17
// Замените for на while без изменения поведения цикла
// for (var i = 0; i < 3; i++) {
//   alert( "номер " + i + "!" );
// }

let i = 0;
while (i < 3) {
    i++;
    alert( "номер " + i + "!" );
}

// Task 18
// Напишите цикл, который предлагает prompt ввести число, большее 100.
// Если пользователь ввёл другое число – попросить ввести ещё раз, и так далее.
// Цикл должен спрашивать число пока либо посетитель не введёт число,
// большее 100, либо не нажмёт кнопку Cancel (ESC).
// Предусматривать обработку нечисловых строк в этой задаче необязательно.

let i = prompt('Введите число > 100');

while (i < 100) {
    i = prompt('Введите число > 100');
}

// Task 19
// Переписать следующий код используя switch
// var a = +prompt('a?', '');
// if (a == 0) {
//   alert( 0 );
// }
// if (a == 1) {
//   alert( 1 );
// }
// if (a == 2 || a == 3) {
//   alert( '2,3' );
// }

var a = +prompt('a?', '');

switch (a) {
   case 0:
       alert(0);
       break;
   case 1:
       alert(1);
       break;
   case 2:
       alert(2);
       break;
   case 3:
       alert(3);
       break;
}

// Task 20
// Объявите переменную и проинициализируйте ее строчным значением в переменном
// регистре. (Например так "таООооОддОО")
// Напишите код, который преобразует эту строку к виду:
// первая буква в верхнем регистре, остальные буквы в нижнем регистре.
// Выведите результат работы в консоль
// Используйте: toUpperCase/toLowerCase, slice.

let str = 'таООооОддОО';
let strUP = str[0].toUpperCase() + str.slice(1, 11).toLowerCase();

console.log(strUP);

// Task 21
// Напишите код, который выводит в консоль true, если строка str содержит
// „viagra“ или „XXX“, а иначе false.
// Тестовые данные: 'buy ViAgRA now', 'free xxxxx'

function addWord(str) {
    let text1 = "XXX";
    let text1Lower = text1.toLowerCase();
    let text2 = "viagra";
    let string = str.toLowerCase();
    let result;

    if (searchWord(text1Lower)||searchWord(text2)) {
        return console.log("true");
    } else {
        return console.log("false");
    }

    function searchWord(word) {
        for (let i = 0; i < string.length - 1; i++) {
            if (string[i] == word[0]) {
                result = string.slice(i, i + word.length);
                if (result == word) {
                    return true;
                }
            }
        }
    }
}

addWord("buy ViAgRA now");


// Task 22
// Напишите код, который проверяет длину строки str, и если она превосходит
// maxlength – заменяет конец str на "...", так чтобы ее длина стала равна maxlength.
// Результатом должна быть (при необходимости) усечённая строка.
// Выведите строку в консоль
// Тестовые данные:
//  "Вот, что мне хотелось бы сказать на эту тему:", 20
//  "Всем привет!", 20

function getMaxLengthStr(str, maxlength) {
    if (str.length > maxlength) {
    str = str.slice(0, maxlength - 1) + "...";
    }
    return str;
}

console.log(maxLengthStr("Вот, что мне хотелось бы сказать на эту тему:", 20));

// Task 23
// Напишите код, который из строки $100 получит число и выведите его в консоль.

console.log(parseInt("$100"))

// Task 24
// Напишите код, который проверит, является ли переменная промисом

console.groupEnd();
