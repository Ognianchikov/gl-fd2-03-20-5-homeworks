function factorialize(num) {
  let result = 1;
  while(num) {
    result *= num--;
  }
  return result;
}

console.log(factorialize(5));
console.log(factorialize(10));
console.log(factorialize(20));
console.log(factorialize(0));